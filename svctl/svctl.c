/**
 * @file svctl.c
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief A user space tool for the secvault kernel module.
 * 
 * This file contains the functions required to perform ioctl commands towards the
 * the secvault control device.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../module/secv.h"

/**
 * @struct arguments
 * @brief Contains the command line arguments
 */
struct arguments {
	/** @var arguments::opt_c
	 * The create option */
	int opt_c;
	/** @var arguments::opt_e
	 * The erase option */
	int opt_e;
	/** @var arguments::opt_d
	 * The delete option */
	int opt_d;
	/** @var arguments::progname 
	 * The command line name by which the program was invoked */
	char * progname;
	/** @var arguments::size
	 * The size of the secvault device */
	unsigned long int size;
	/** @var arguments::id
	 * Id of the secvault device */
	unsigned short int id;	
};

/** @brief Holds the parsed command line arguments */
struct arguments args = {
	.progname = "svctl",
	.opt_c = 0,
	.opt_e = 0,
	.opt_d = 0,
	.size  = 0,
	.id    = 0
};

/** @brief The control device's file descriptor */
static int fctl = -1;

/** @brief Prints the program's synopsis to stdout */
void usage(void);

/** 
 * @brief Prints an error message, leaves the program with the code retval.
 * @param retval The return code
 * @param fmt The format string for the error message
 * @param ... Arguments for the error message
 * @return void
 * @details If errno is not equal to 0, this function also prints an errno specific error
 * message.
 */
void bail_out(int ret, const char * fmt, ...);

/**
 * @brief Parses the command line arguments.
 * @param argc The number of command line arguments (including the program name)
 * @param argv A pointer to the command line argument array
 * @return void
 * @details Saves the parsed arguments to ::args
 */
void parse_arguments(int argc, char ** argv);

/**
 * @brief Frees the ressources allocated by the program
 * @details Frees the control devices file descriptor ::fctl
 */
void free_resources(void);

/**
 * @brief Returns constant of the ioctl command specified by the command line arguments
 * @param Pointer to the command line arguments
 * @return The constant of the requested ioctl command
 */
int get_ioctl_action(const struct arguments args);

/**
 * @brief Performs an ioctl command
 * @param fctl File descriptor of the device
 * @param cmd The request code
 * @param dev A pointer to the arguments
 */
void ioctl_action(int fctl, int cmd,
		const struct secv_info * dev);

/**
 * @brief Reads the devices encryption key from stdin
 * @param buf The buffer to store the key in
 */
void read_key(char *buf);

/**
 * @brief The programs entry point
 * @param argc The number of of command line arguments
 * @param argv Pointer to the command line arguments array
 * @return the programs exit code
 */
int main(int argc, char ** argv)
{
	struct secv_info dev;

	parse_arguments(argc, argv);

	dev.id = args.id;
	
	if (args.opt_c) {
		dev.size = args.size;
		read_key(dev.key);
	}

	if (args.opt_c || args.opt_e || args.opt_d) {
		if (-1 == (fctl = open(SECV_CTL_DEV, 0))) {
			bail_out(EXIT_FAILURE, "open()");
		}

		ioctl_action(fctl, get_ioctl_action(args), &dev);
	}

	free_resources();

	return 0;
}

void parse_arguments(int argc, char ** argv)
{
	char c;

	if (argc >= 1) {
		args.progname = argv[0];
	}

	while (-1 != (c = getopt(argc, argv, "c:ed"))) {
		switch (c) {
			case 'c':
			if (args.opt_e || args.opt_d || NULL == optarg) {
				usage();
				bail_out(EXIT_FAILURE, "invalid options");
			}	
			args.opt_c = 1;

			char *endptr = NULL;
			args.size = strtol(optarg, &endptr, 10);
			if ('\0' != *endptr) {
				usage();
				bail_out(EXIT_FAILURE, "invalid arguments");
			}
			break;

			case 'e':
			if (args.opt_c || args.opt_d) {
				usage();
				bail_out(EXIT_FAILURE, "invalid options");
			}
			args.opt_e = 1;
			break;

			case 'd':
			if (args.opt_e || args.opt_c) {
				usage();
				bail_out(EXIT_FAILURE, "invalid options");
			}
			args.opt_d = 1;
			break;
		}
	}
	
	if (optind != argc - 1) {
		usage();
		bail_out(EXIT_FAILURE, "invalid arguments");
	}
	
	char *endptr = NULL;
	args.id = strtol(argv[optind], &endptr, 10);	
	
	if ('\0' != *endptr) {
		usage();
		bail_out(EXIT_FAILURE, "invalid arguments");
	}
}

void usage(void)
{
	(void) fprintf(stdout, "usage: %s [-c <size>|-e|-d] <secvault id>\n",
			args.progname);
}

void bail_out(int ret, const char * fmt, ...)
{
	va_list ap;

	(void) fprintf(stderr, "%s : ", args.progname);

	va_start(ap, fmt);
	(void) vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (0 != errno) {
		(void) fprintf(stderr, " : %s", strerror(errno));	
	}
	
	(void) fputc('\n', stderr);

	free_resources();

	exit(ret);
}

void free_resources(void)
{
	(void) close(fctl);	
}

void ioctl_action(int fctl, int ioctl_action,
		const struct secv_info * dev)
{
	int ret = ioctl(fctl, ioctl_action, (void *) dev);
	if (-1 == ret) {
		bail_out(EXIT_FAILURE, "ioctl()");
	}
} 

int get_ioctl_action(const struct arguments args)
{
	int action = 0;

	if (args.opt_c) { 
		action = SECV_IOCTL_CREATE;
	} else if (args.opt_e) {
		action = SECV_IOCTL_ZERO;
	} else {
		action = SECV_IOCTL_DELETE;
	}

	return action;
}

void read_key(char *buf)
{
	char tmp[SECV_KEY_LEN + 1];	

	if (NULL == fgets(tmp, SECV_KEY_LEN + 1, stdin)) {
		bail_out(EXIT_FAILURE, "fgets()");
	}

	if (tmp[strlen(tmp) - 1] == '\n') {
		tmp[strlen(tmp) - 1] =  '\0';	
	}
	
	bzero(buf, SECV_KEY_LEN);
	memcpy(buf, tmp, strlen(tmp));
}
