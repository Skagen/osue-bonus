/**
 * @file secv_ioctl.h 
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief Contains declarations required by other modules.
 */
#ifndef DEF_SECV_IOCTL_H
#define DEF_SECV_IOCTL_H

/**
 * @brief Creates a new secvault with the properties specfied by userptr
 * @param userptr Userspace pointer to a secv_info structure
 * @return returns 0 on success, and a negative value on error
 * @details Access to userptr must be checked before calling this function
 * @details The newly created secvault is owned by the owner of the calling
 * Process.
 */
int secv_ioctl_create(unsigned long userptr);

/**
 * @brief Removes a secvault device with the id specified in the structure
 * pointed by userptr
 * @param userptr A userspace pointer to a secv_info structure
 * @return returns 0 on success, a negative value on error.
 * @details The access to userptr must be checked before calling this function
 */
int secv_ioctl_zero(unsigned long userptr);

/**
 * @brief Erases the entire memory of a secvault device with null bytes
 * @param userptr A userspace pointer to a secv_info structure
 * @return returns 0 on success, otherwise a negated error code is returned
 */
int secv_ioctl_remove(unsigned long userptr);

#endif
