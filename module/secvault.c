/**
 * @file secvault.c
 * @author Jannik Vierling 1226434
 * @date 20/12/2013
 * 
 * @brief Contains the functions required to set up the driver for the secvault
 * control device, as well as the secvault devices.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/semaphore.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/capability.h>
#include "secv_module.h"
#include "secv_ioctl.h"
#include "secv_fops.h"

MODULE_LICENSE("GPL");

/**
 * @brief Frees the resources allocated for the secvault device dev
 * @param dev The device to be freed
 * @details This function is not reentrant
 */
static void secv_free_device(struct secv_dev *dev);

/**
 * @brief Initializes the secvault device structures
 * @details This function must be called before performing any operations on
 * the devices semaphores.
 */
static void secv_init_devices(void);

/**
 * @brief Sets up the secvault control device
 * @param sdev A pointer to the control device's structure
 */
static int secv_setup_ctl(struct secv_ctl_dev *sdev);

/**
 * @brief Copies count bytes from src to dest
 * @param dest The destination
 * @param src The source
 * @param count The number of bytes to copy
 */
static void *my_memcpy(void *dest, const void *src, size_t count);

/**
 * @brief Removes a device regadless of its owner
 * @param dev A pointer to the device to be removed
 */
static void secv_remove_dev_force(struct secv_dev * dev);

/**
 * @brief Holds the error code of errors that may occur during
 * initilization
 */
static int error = 0;

/**
 * @brief This module parameter indicates whether the driver performs debug
 * output
 */
int debug = 1;

module_param(debug, int, S_IRUGO);

/** @brief The secvault device structures */
struct secv_dev devices[SECV_DEVICES];

/** @brief The secvault control device structure */
struct secv_ctl_dev secv_ctl;

/**
 * @brief Initilizes the secvault module
 * @return Returns a value less than 0 if errors occured, else it returns 0
 * @details Allocates device numbers for the control device, and the secvault
 * devices.
 */
static int secv_init(void)
{
	int retval  = 0;
	dev_t first = MKDEV(SECV_MAJOR, 0);

	print_debug("registering device numbers...");

	retval = register_chrdev_region(first, SECV_NR_DEV, SECV_NAME);
	if (0 > retval) {
		print_error("could not register device numbers");
		error = ECHAR_REGION;
		goto leave;
	}
	
	retval = secv_setup_ctl(&secv_ctl);
	if (0 > retval) {
		error = ECHAR_ADD;
		goto leave;
	}

	secv_init_devices();
	
	leave:
		return retval;
}
module_init(secv_init);

/**
 * @brief Frees the devices and their resources.
 */
static void secv_exit(void)
{
	int i;
	dev_t first = MKDEV(SECV_MAJOR, 0);

	if (error == ECHAR_REGION) goto leave_1;

	if (error == ECHAR_ADD)    goto leave_2;

	cdev_del(&secv_ctl.cdev);
	print_debug("freed control device");

	leave_2:
		for (i = 0; i < SECV_DEVICES; i++) {
			secv_remove_dev_force(&devices[i]);		
		}
		unregister_chrdev_region(first, SECV_NR_DEV);
		print_debug("unregistered device numbers");

	leave_1:
		return;
}
module_exit(secv_exit);

static void secv_init_devices(void)
{
	int i;

	for (i = 0; i < SECV_DEVICES; i++) {
		sema_init(&devices[i].sem, 1);
		devices[i].exists = 0;
		devices[i].mem    = NULL;
		devices[i].id     = i;
		devices[i].size   = 0;
		memset(devices[i].key, 0x00, SECV_KEY_LEN);
	}
}

static int secv_setup_ctl(struct secv_ctl_dev *sdev)
{
	int error = 0;

	print_debug("setting up control device...");

	sdev->num  = MKDEV(SECV_MAJOR, SECV_CTL_MINOR);

	cdev_init(&sdev->cdev, &secv_ctl_fops);
	sdev->cdev.owner = THIS_MODULE;
	sdev->cdev.ops   = &secv_ctl_fops;

	error = cdev_add(&sdev->cdev, sdev->num, 1);
	if (error) {
		print_error("could not add control device\n");
	}

	return error;
}

static void *my_memcpy(void *dest, const void *src, size_t count)
{
	int i;

	for (i = 0; i < count; i++) {
		((char *)dest)[i] = ((char *)src)[i];
	}

	return dest;
}

static void secv_remove_dev_force(struct secv_dev * dev)
{
	if (down_interruptible(&dev->sem))  return;

	if (dev->exists) {
		print_debug("freed sevault %d (forced)", dev->id);
		secv_free_device(dev);
	}

	up(&dev->sem);
}

static void secv_free_device(struct secv_dev * dev)
{		
	cdev_del(&dev->cdev);
	kfree(dev->mem);
	dev->exists = 0;
}

int secv_create_dev(const struct secv_info * info)
{
	if (down_interruptible(&devices[info->id].sem)) return -ERESTARTSYS;
	
	/* check if the device has already been allocated */
	if (devices[info->id].exists) {
		print_error("secv_create_dev() : device already exists");
		goto error_1;	
	}

	/* allocate devices memory */
	devices[info->id].mem = (char *) kmalloc(info->size, GFP_KERNEL);
	if (NULL == devices[info->id].mem) {
		print_error("secv_create_dev() : could not allocate device "
				"memory");
		goto error_1;	
	}

	/* set device properties, zero device memory */
	devices[info->id].size   = info->size;
	devices[info->id].exists = 1;
	devices[info->id].owner  = current_uid();
	my_memcpy(devices[info->id].key, info->key, SECV_KEY_LEN);
	memset(devices[info->id].mem, 0x00, info->size);	
	
	/* create and add cdev */
	cdev_init(&devices[info->id].cdev, &secv_dev_fops);
	devices[info->id].cdev.owner = THIS_MODULE;

	if (0 > cdev_add(&devices[info->id].cdev,
			MKDEV(SECV_MAJOR, info->id + 1), 1)) {
		print_error("secv_create_dev() : could not add char device");
		goto error_2;
	}

	up(&devices[info->id].sem);
	
	return 0;

	error_2:
		devices[info->id].exists = 0;
		kfree(devices[info->id].mem);

	error_1:
		up(&devices[info->id].sem);

	return -1;
}

int device_info_ok(struct secv_info *info)
{
	if (info->size < SECV_SIZE_MIN || info->size > SECV_SIZE_MAX) {
		return 0;
	}

	if (info->id < 0 || info->id > SECV_ID_MAX) {
		return 0;	
	}

	return 1;
}

int secv_zero_dev(struct secv_dev * dev)
{	
	int retval = 0;

	print_debug("zeroing secvault %d", dev->id);

	if (down_interruptible(&dev->sem)) return -ERESTARTSYS;

	if (!dev->exists) {
		print_error("secv_zero_dev() : secvault %d does not exist",
				dev->id);
		retval = -1;
		goto leave;
	}

	if (!is_allowed(dev)) {
		print_error("secv_zero_dev() : permission denied,"
				"uid(%d) euid(%d)",
				current_uid(), current_euid());
		retval = -EPERM;
		goto leave;
	}

	memset(dev->mem, 0x00, dev->size);

	leave:
		up(&dev->sem);

	return retval;
}

int secv_remove_dev(struct secv_dev * dev)
{
	int retval = 0;

	if (down_interruptible(&dev->sem))  return -ERESTARTSYS;

	if (!dev->exists) {
		print_error("secv_zero_dev() : secvault %d does not exist",
				dev->id);
		retval = -1;
		goto leave;
	}

	if (!is_allowed(dev)) {
		print_error("secv_remove_dev() : permission denied, "
				"uid(%d), euid(%d)",
				current_uid(), current_euid());
		retval = -EPERM;
		goto leave;	
	}
	
	print_debug("freed sevault %d", dev->id);
	secv_free_device(dev);

	leave:
		up(&dev->sem);

	return retval;
}

int is_allowed(const struct secv_dev * dev)
{
	return dev->exists
			&& (dev->owner == current_uid()
				|| dev->owner == current_euid()
				|| capable(CAP_DAC_OVERRIDE));
}
