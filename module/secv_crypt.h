/**
 * @file secv_crypt.h
 * @author Jannik Vierling 1226434
 * @date 20/12/2013
 * 
 * @brief Contains the definitions of the secvault crypting module.
 */

#ifndef DEV_SECV_CRYPT
#define DEV_SECV_CRYPT

/**
 * @brief Encrypts bytes of from and saves them to the device's memory
 * @param dev The secvault device
 * @param from The bytes to encrypt
 * @param count The number of bytes to encrypt
 * @param pos The offset where the encrypted bytes are saved within the devices
 * memory
 * @details The bytes are encrypted with an xor-cipher.
 */
void secv_crypt(struct secv_dev * dev, const void * from, size_t count,
		unsigned long pos);

/**
 * @brief Decrypts bytes from the device and saves them to to
 * @param dev The secvault device
 * @param to The destination of the decrypted bytes
 * @param count The number of bytes to decrypt
 * @param pos The offset, where to begin decrypting bytes
 * @ return a pointer to the memory area to
 */
void * secv_decrypt(const struct secv_dev * dev, void * to, size_t count,
		unsigned long pos);

#endif
