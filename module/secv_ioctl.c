/**
 * @file secv_ioctl.c
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief Contains the implementation of the secvault ioctl requests. 
 */
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/slab.h>
#include "secv_ioctl.h"
#include "secv_module.h"

int secv_ioctl_create(unsigned long userptr)
{
	struct secv_info * info = kmalloc(sizeof(struct secv_info),
			GFP_KERNEL);
	int retval = 0;
	
	if (NULL == info) {
		print_error("secv_setup_dev() : could not allocate "
			"info struct");
		retval = -1;
		goto leave;
	}

	if (copy_from_user(info, (void *)userptr,
			sizeof(struct secv_info)) != 0) {
		print_error("secv_setup_dev() : could not copy from user");
		retval = -EFAULT;
		goto leave;
	}

	print_debug("creating secvault %d with size = %lu",
			info->id, info->size);

	if (!device_info_ok(info)) {
		print_error("secv_setup_dev() : invalid device information");
		retval = -EINVAL;
		goto leave;
	}

	if (secv_create_dev(info) != 0) {
		retval = -1;
	}

	leave:
		kfree(info);

	return retval;
}

int secv_ioctl_remove(unsigned long userptr)
{
	struct secv_info * info = kmalloc(sizeof(struct secv_info),
			GFP_KERNEL);
	int retval              = 0;

	if (NULL == info) {
		print_error("secv_remove_dev() : could not allocate memory");
		retval = -1;
		goto leave;
	}
	
	if (0 != copy_from_user(info, (void *)userptr,
			sizeof(struct secv_info))) {
		print_error("secv_remove_dev() : could not copy from user");
		retval = -EFAULT;		
		goto leave;
	}
	
	if (0 > info->id || info->id > SECV_ID_MAX) {
		print_error("secv_remove_dev() : invalid device information");
		retval = -EINVAL;
		goto leave;
	}

	retval = secv_remove_dev(&devices[info->id]);

	leave:
		kfree(info);

	return retval;
}

int secv_ioctl_zero(unsigned long userptr)
{
	struct secv_info * info = kmalloc(sizeof(struct secv_info),
			GFP_KERNEL);
	int retval              = 0;

	if (NULL == info) {
		print_error("secv_zero_dev() : could not allocate memory");
		retval = -1;
		goto leave;
	}

	if (copy_from_user(info, (void *)userptr,
			sizeof(struct secv_info)) != 0) {
		print_error("secv_zero_dev() : could not copy from user");
		retval = -EFAULT;
		goto leave;	
	}

	if (info->id < 0 || info->id > SECV_ID_MAX) {
		print_error("secv_zero_dev() : invalid device information");
		retval = -EINVAL;
		goto leave;
	}

	retval = secv_zero_dev(&devices[info->id]);

	leave:
		kfree(info);

	return retval;
}

