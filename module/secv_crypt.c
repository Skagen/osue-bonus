/**
 * @file secv_crypt.c
 * @author Jannik Vierling 1226434
 * @date 20/12/2013
 * 
 * @brief Contains the functions to encrypt and decrypt the secvault memory
 */
#include <linux/types.h>
#include "secv.h"
#include "secv_module.h"

void secv_crypt(struct secv_dev * dev, const void * from, size_t count,
		unsigned long pos)
{
	char * src = (char *) from;
	int i = 0;

	for (i = 0; i < count; i++) {
		dev->mem[pos+ i] = src[i] ^ dev->key[(pos+i) % SECV_KEY_LEN];	
	}
}

void * secv_decrypt(const struct secv_dev * dev, void * to, size_t count,
		unsigned long pos)
{
	char * dest = (char *) to;
	int i = 0;

	for (i = 0; i < count; i++) {
		dest[i] = dev->mem[i + pos]
			^ dev->key[(i + pos) % SECV_KEY_LEN];
	}

	return to;
}

