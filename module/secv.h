/**
 * @file secv.h
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief Header file of the secvault module 
 * 
 * This file contains all symbols that are required by user space applications
 * to use the secvault module.
 */
#ifndef DEF_SECV_H
#define DEF_SECV_H

/** @brief The minimum size of secvault device */
#define SECV_SIZE_MIN (1)
/** @brief The maximum size of a secvault device */
#define SECV_SIZE_MAX (1048576)

/** @brief The number of secvault devices */
#define SECV_DEVICES (4)

/** @brief The maximum id of a secvault device */
#define SECV_ID_MAX (3)

/** @brief Path to the control device */
#define SECV_CTL_DEV "/dev/sv_ctl"

/** @brief Number of bytes of the secvault encryption key */
#define SECV_KEY_LEN (10)

/**
 * @struct secv_info
 * @brief Contains the properties of a secvault device
 */
struct secv_info {
	/** @var secv_info::id
	 * The secvaults device id */
	unsigned short int id;
	/** @var secv_info::size
	 * The secvault device size */
	unsigned long int size;
	/** @var secv_info::key
	 * The secvault encryption key */
	char key[SECV_KEY_LEN];
};

#include <linux/ioctl.h>

/** @brief The magic number used to generate the ioctl request codes */
#define SECV_IO_MAGIC 's'

/** 
 * @brief The ioctl request code for the secvault device creation
 * @details The device properties are passed by a pointer to a secv_info structure
 */
#define SECV_IOCTL_CREATE _IOW(SECV_IO_MAGIC, 0, struct secv_info)

/** 
 * @brief The ioctl request code that deletes a secvault device
 * @details The id of the device to be deleted is passed in a secv_info structure
 */
#define SECV_IOCTL_DELETE _IOW(SECV_IO_MAGIC, 1, struct secv_info)

/**
 * @brief The ioctl request code that erases the secvault device's memory
 * @details The id of the device to be erased is passed in a secv_info structure
 */
#define SECV_IOCTL_ZERO	  _IOW(SECV_IO_MAGIC, 2, struct secv_info)

#endif
