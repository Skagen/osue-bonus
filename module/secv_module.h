/**
 * @file secv_module.h
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief Header file of the secvault module 
 * 
 * This file contains all symbols that are required by the modules composing
 * the Secvault module.
 */
#ifndef DEF_SECV_MODULE_H
#define DEF_SECV_MODULE_H

#include <linux/fs.h>
#include <linux/types.h>
#include <linux/semaphore.h>
#include <linux/cdev.h>

#include "secv.h"

#define ECHAR_REGION 	(1)
#define ECHAR_ADD    	(2)

/** @brief The control device minor number */
#define SECV_CTL_MINOR 	(0)

/** @brief The first secvault device minor number */
#define SECV_DEV_MINOR 	(1)

/** @brief The control device's and the secvault device major number */
#define SECV_MAJOR		(231)
/** @brief Total number of devices */
#define SECV_NR_DEV 	(5)
/** @brief The device name that is used to allocate the device number region */
#define SECV_NAME		"sec_vault"

/** 
 * @brief Prints a debug message
 * @param fmt format string
 * @param args A variable number of arguments to format in output
 */
#define print_debug(fmt, args...) \
	do { \
		if (debug) \
			printk(KERN_ALERT "%s : debug : " fmt "\n",\
					THIS_MODULE->name, ##args);\
	} while(0)

/** 
 * @brief Prints an error message
 * @param fmt format string
 * @param args A variable number of arguments to format in output
 */
#define print_error(fmt, args...) \
	do { \
		printk(KERN_ALERT "%s : error : " fmt "\n",\
				THIS_MODULE->name, ##args);\
	} while(0)

/** @brief The module parameter which indicates whether to do debug output
 * or not */
extern int debug;

/**
 * @struct secv_ctl_dev
 * @brief Contains the information about a control device
 */
struct secv_ctl_dev {
	/** @var secv_ctl_dev::num
	 * The device number */
	dev_t num;
	/** @var cdev::secv_ctl_dev::cdev
	 * The char device structure */
	struct cdev cdev;
};

/**
 * @struct secv_dev
 * @brief Contains the information about a secvault device
 */
struct secv_dev {
	/** @var secv_dev::exists
	 * Indicates whether the device is allocated */
	int exists;
	/** @var secv_dev::owner
	 * The uid of the device's owner */
	uid_t owner;
	/** @var secv_dev::cdev
	 * The device's char device structure */
	struct cdev cdev;
	/** @var secv_dev::sem
	 * The devices mutex */
	struct semaphore sem;
	/** @var secv_dev::key
	 * The encryption key */
	char key[SECV_KEY_LEN];
	/** @var secv_dev::size
	 * The secvaults size in bytes */
	unsigned long size;
	/** @var secv_dev::id
	 * The devices id */
	int id;
	/** @var secv_dev::mem
	 * Pointer to the memory region (size bytes) allocated for this 
	 * device*/
	char * mem;
};

/** @brief The secvault devices structures */
extern struct secv_dev devices[SECV_DEVICES];
/** @brief The control device structures */
extern struct secv_ctl_dev secv_ctl;

/**
 * @brief Overwrites the entire memory of the device dev with null bytes
 * @param dev The secvault device
 * @return returns 0 on success, othwise a negative value is returned
 */
int secv_zero_dev(struct secv_dev * dev);

/**
 * @brief Checks if the device information info is valid
 * @param dev The device information structure
 * @return returns 1 if the device information is valid, otherwise 0 is
 * returned.
 * @details This function is not reentrant
 */
int device_info_ok(struct secv_info *info);

/**
 * @brief Checks whether the current process is allowed to access the secvault
 * device dev.
 * @param dev The secvault device structure
 * @return returns 1 if the process is allowed to access the device, otherwise
 * the function returns 0.
 */
int is_allowed(const struct secv_dev * dev);

/**
 * @brief Removes the secvault device dev
 * @param dev The secvault device structure of the device to be removed
 * @return returns 0 if the device could be removed, otherwise the function
 * returns a negative value.
 */
int secv_remove_dev(struct secv_dev * dev);

/**
 * @brief Creates a new secvault device with the device information info
 * @param info The device information
 * @return returns 0 if the device creation was successfull, otherwise it
 * returns a negative value.
 */
int secv_create_dev(const struct secv_info * info);

#endif
