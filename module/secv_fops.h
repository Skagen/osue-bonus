/**
 * @file secv_fops.h
 * @author Jannik Vierling 1226434
 * @date 20.12.2013
 * 
 * @brief Header file for the secv_fops module.
 */
#ifndef DEF_SECV_FOPS_H
#define DEF_SECV_FOPS_H

#include <linux/fs.h>

#include "secv_module.h"

/** @brief The control device file operations */
extern struct file_operations secv_ctl_fops;

/** @brief The secvault device file operations */
extern struct file_operations secv_dev_fops; 

#endif
