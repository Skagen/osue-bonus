/**
 * @file secv_fops.c
 * @author Jannik Vierling 1226434
 * @date 20/12/2013
 * 
 * @brief Contains the file operations functions required for the control
 * device and the secvault devices.
 */
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/slab.h>
#include "secv_fops.h"
#include "secv_ioctl.h"
#include "secv_crypt.h"
#include "secv_module.h"

/**
 * @brief Opens a control device
 * @param in Inode structure of the device file
 * @param filp File structure of the opened file
 * return this function always returns 0
 */
static int secv_ctl_open(struct inode *in, struct file *filp);

/**
 * @brief Releases a control device
 * @param in Inode structure of the device file
 * @param filp The file structure of the device file
 * @return This function always returns 0
 */
static int secv_ctl_release(struct inode *in, struct file *filp);

/**
 * @brief Changes the current read/write position of the file
 * @param filp The file structure of the device file
 * @param off The offset of the new positon
 * @param whence The location of the new position
 * @return returns the new position, errors are signaled by a negative return
 * value.
 */
static loff_t secv_dev_llseek(struct file * filp, loff_t off, int whence);

/**
 * @brief Opens a secvault device
 * @param in The inode structure of the device file
 * @param filp The file structure of the device file 
 * @return returns 0 if the device could be opened, else it returns a negative
 * value.
 */
static int secv_dev_open(struct inode * in, struct file * filp);

/**
 * @brief Closes an opened secvault device
 * @param in The inode structure of the device file
 * @param filp The file structure of the device file
 * @return returns always 0
 */
static int secv_dev_release(struct inode * in, struct file * filp);

/** 
 * @brief Reads from an opened secvault device
 * @param filp The file structure of the device file
 * @param buf Pointer to a memory area in user space where the read bytes
 * are saved
 * @param count The number of bytes to read
 * @param f_pos The position where to read bytes
 * @return returns the number of bytes read, 0 indicates the end of file,
 * neagtive values signal an error
 * @details If the calling process does not belong to the user who created
 * the device, this function fails with the code -EPERM 
 */
static ssize_t secv_dev_read(struct file * filp, char __user* buf,
		size_t count, loff_t *f_pos);

/**
 * @brief Writes to an opened secvault device
 * @param filp The file structure of the device file
 * @param buf Pointer to a memory area in user space which contains the bytes
 * to write
 * @param count The number of bytes to write
 * @param f_pos The position of the device where the bytes are written
 * @return returns the number of bytes written, a negative value signals an
 * error.
 * @details If the calling process does not belong to the user who created
 * the device, this function fails with the code -EPERM 
 */
static ssize_t secv_dev_write(struct file * filp, const char __user* buf,
		size_t count, loff_t * f_pos);

/**
 * @brief Handles ioctl commands
 * @param filp The file structure of the opened device
 * @param cmd The ioctl request code
 * @param arg A pointer to the arguments in user space
 * @return returns 0 if no errors happened, else the function returns a
 * negative value.
 */
static long secv_ctl_ioctl(struct file *filp, unsigned int cmd,
		unsigned long arg);

/** @brief The secvault device file operations */
struct file_operations secv_dev_fops = {
	.owner   = THIS_MODULE,
	.open  	 = secv_dev_open,
	.release = secv_dev_release,
	.read    = secv_dev_read,
	.write   = secv_dev_write,
	.llseek  = secv_dev_llseek
};	

/** @brief The control device file operations */
struct file_operations secv_ctl_fops = {
	.owner   = THIS_MODULE, 
	.unlocked_ioctl = secv_ctl_ioctl,
	.open    = secv_ctl_open, 
	.release = secv_ctl_release
};

int secv_ctl_open(struct inode *in, struct file *filp)
{
	struct secv_ctl_dev * dev;

	dev = container_of(in->i_cdev, struct secv_ctl_dev, cdev);
	filp->private_data = (void *) dev;

	print_debug("opened control device");

	return 0;
}

int secv_ctl_release(struct inode *in, struct file *filp)
{
	print_debug("closed control device");

	return 0;
}

int secv_dev_open(struct inode * in, struct file * filp)
{
	struct secv_dev * dev = NULL;
	int retval            = 0;

	dev = container_of(in->i_cdev, struct secv_dev, cdev);

	if (down_interruptible(&dev->sem)) return -ERESTARTSYS;

	if (is_allowed(dev)) {
		filp->private_data = (void *)dev;
		print_debug("opened secvault %d", dev->id);
	} else {
		retval = -1;	
	}	

	up(&dev->sem);

	return retval;
}

int secv_dev_release(struct inode * in, struct file * filp)
{
	print_debug("closed secvault %d",
			((struct secv_dev *) filp->private_data)->id);
	
	return 0;
}

ssize_t secv_dev_read(struct file * filp, char __user* buf,
		size_t count, loff_t *f_pos)
{
	struct secv_dev * dev = (struct secv_dev *)filp->private_data;
	char * kbuffer        = NULL;
	int retval 	      = count;
	
	if (down_interruptible(&dev->sem))  return -ERESTARTSYS;

	/* device does't exist anymore */
	if (!dev->exists) {
		retval = -EBADF;
		goto leave;
	}

	if (*f_pos < 0 || *f_pos >= dev->size) {
		print_debug("secvault %d : read EOF at pos %lld",
				dev->id, *f_pos);
		retval = 0;
		goto leave;
	}

	if (*f_pos + count > dev->size) {
		retval = count = dev->size - *f_pos;	
	}

	if (NULL == (kbuffer = kmalloc(count, GFP_KERNEL))) {
		retval = -EFAULT;
		goto leave;
	}
	secv_decrypt(dev, kbuffer, count, *f_pos);

	if (copy_to_user(buf, kbuffer, count) != 0) {
		retval = -EFAULT;
		goto error_1;	
	}

	print_debug("secvault %d : read %zu bytes at pos %lld",
			dev->id, count, *f_pos);

	/* update file cursor */
	*f_pos += count;

	error_1:
		kfree(kbuffer);

	leave:
		up(&dev->sem);

	return retval;
}

ssize_t secv_dev_write(struct file * filp, const char __user* buf,
		size_t count, loff_t * f_pos)
{
	struct secv_dev * dev = (struct secv_dev *) filp->private_data;
	char * kbuffer        = NULL;
	int retval	      = count;

	if (down_interruptible(&dev->sem)) return -ERESTARTSYS;

	if (!dev->exists) {
		retval = -EBADF;
		goto leave;
	}

	if (*f_pos < 0 || *f_pos >= dev->size) {
		print_error("secvault %d : tried to write at pos %lld",
				dev->id, *f_pos);
		retval = -EFBIG;
		goto leave;
	}

	if (*f_pos + count > dev->size) {
		count = dev->size - *f_pos;	
	}
	
	if (NULL == (kbuffer = kmalloc(count, GFP_KERNEL))) {
		retval = -EFAULT;
		goto leave;
	}
	if (copy_from_user(kbuffer, buf, count) != 0) {
		retval = -EFAULT;
		goto error_1;	
	}
	secv_crypt(dev, kbuffer, count, *f_pos);

	print_debug("secvault %d : wrote %zu bytes at pos %lld",
			dev->id, count, *f_pos);

	/* update file cursor */
	*f_pos += count;

	error_1:
		kfree(kbuffer);

	leave:
		up(&dev->sem);

	return retval;
}

loff_t secv_dev_llseek(struct file * filp, loff_t off, int whence)
{
	struct secv_dev * dev = (struct secv_dev *) filp->private_data;

	loff_t newpos = 0;

	switch (whence) {
		case SEEK_SET:
		newpos = off;
		break;

		case SEEK_CUR:
		newpos = filp->f_pos + off;
		break;
	
		case SEEK_END:
		newpos = dev->size + off;
		break;

		default:
		return -EINVAL;
	}

	if (newpos < 0) {
		return -EINVAL;	
	}
	filp->f_pos = newpos;

	print_debug("secvault %d : moved cursor to pos %lld", dev->id, newpos);
	
	return newpos;
}

long secv_ctl_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	long ret = 0;

	print_debug("ioctl command, cmd = %u", cmd);
	
	switch (cmd) {
		case SECV_IOCTL_CREATE:
		if (access_ok(VERIFY_READ, arg, _IOC_SIZE(cmd))) {
			ret = secv_ioctl_create(arg);
		} else {
			ret = -EFAULT;
		}
		break;
	
		case SECV_IOCTL_ZERO:
		if (access_ok(VERIFY_READ, arg, _IOC_SIZE(cmd))) {
			ret = secv_ioctl_zero(arg);
		} else {
			ret = -EFAULT;
		}
		break;

		case SECV_IOCTL_DELETE:
		if (access_ok(VERIFY_READ, arg, _IOC_SIZE(cmd))) {
			ret = secv_ioctl_remove(arg);
		} else {
			ret = -EFAULT;
		}
		break;
		
		default:
		ret = -ENOTTY;
		print_error("unknow ioctl command");
	}

	return ret;
}

